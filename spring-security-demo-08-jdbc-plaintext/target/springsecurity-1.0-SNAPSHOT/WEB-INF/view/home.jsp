<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="section" uri="http://www.springframework.org/security/tags" %>

<html>
<head><title>Gent mvc testing with spring security</title></head>

<body>
<h2 class="title">WELCOME</h2>

<hr>
<p>
    <%-- Display user name and Role --%>
    User: <security:authentication property="principal.username"/>
    <br><br>
    Role (s): <security:authentication property="principal.authorities"/>
<hr>
</p>



<security:authorize access="hasRole('MANAGER')">
    <hr>
    <%--Add a link to point to /leaders ... this is for the managers --%>
    <p>
        <a href="${pageContext.request.contextPath}/leaders"> LeaderShip Meeting</a>
        (Only for Manager peeps)
    </p>

</security:authorize>



<section:authorize access="hasRole('ADIN')">
    <hr>
    <%--Add a link to point to /systems ... this is for ADMIN--%>
    <hr>
    <p>
        <a href="${pageContext.request.contextPath}/systems"> Admin notes</a>
    </p>
    (Only for Admin peeps)
</section:authorize>


<%--Add logout button--%>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input type="submit" value="Logout"/>
</form:form>


</body>
</html>