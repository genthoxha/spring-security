<%--
  Created by IntelliJ IDEA.
  User: genthoxha
  Date: 12/25/2018
  Time: 1:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Custom login form</title>
    <style>
        .failed {
            color: red;
        }
    </style>
</head>
<body>

<h3> My custom login page</h3>
<%-- Because of form:form tokens added from spring--%>
<%--<form:form action="${pageContext.request.contextPath}/authenticateTheUser"
           method="POST">

    &lt;%&ndash;Check for login error&ndash;%&gt;

    <c:if test="${param.error != null}">
        <i class="failed"> Sorry! You entered invalid username/password.</i>
    </c:if>
    <p>
        User name: <input type="text" name="username"/>
    </p>

    <p>
        User name: <input type="password" name="password"/>
    </p>

    <input type="submit" value="Login"/>
</form:form>--%>

<%--Add tokens manually--%>
<form action="${pageContext.request.contextPath}/authenticateTheUser"
      method="POST">

    <%--Check for login error--%>

    <c:if test="${param.error != null}">
        <i class="failed"> Sorry! You entered invalid username/password.</i>
    </c:if>
    <p>
        User name: <input type="text" name="username"/>
    </p>

    <p>
        User name: <input type="password" name="password"/>
    </p>
    <input type="submit" value="Login"/>
    <%--I'm manually adding tokens bro--%>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
</form>
</body>
</html>
