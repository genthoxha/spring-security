<%--
  Created by IntelliJ IDEA.
  User: genthoxha
  Date: 12/25/2018
  Time: 12:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Leaders Home Page</title>
</head>
<body>
    <h2>Leaders Home Page</h2>
    <hr>
    <p>
        See you in Brazil ... for our annual Leadership retreat !
        <br>
        Keep this trip a secret, don't tell the regular employees :-)
    </p>

<a href="${pageContext.request.contextPath}/">Back to Home Page</a>
</body>
</html>
